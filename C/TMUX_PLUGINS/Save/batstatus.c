#include <stdio.h>

#define POWER_SUPPLY_CAPACITY "/sys/class/power_supply/BAT1/capacity"
#define POWER_SUPPLY_STATUS "/sys/class/power_supply/BAT1/status"
#define POWER_SUPPLY_CHARGE "/sys/class/power_supply/BAT1/charge_now"
#define POWER_SUPPLY_ENERGY "sys/class/power_suppply/BAT1/energy_now"
#define POWER_SUPPLY_CURRENT "/sys/class/power_supply/BAT1/current_now"
#define POWER_SUPPLY_POWER "/sys/class/power_supply/BAT1/power_now"

const int battery_percent() {
	int cap_perc;
	FILE *fid = fopen(POWER_SUPPLY_CAPACITY,"r");
	if(fid == NULL) { fprintf(stderr,"Error opening file\n"); }
	fscanf(fid,"%d",&cap_perc);
	fclose(fid);

	return cap_perc;
}

const int battery_remaining() {
	// Read charge_now
	int charge_now;

	FILE *f_charge = fopen(POWER_SUPPLY_CHARGE,"r");
	if (f_charge == NULL) { fprintf(stderr,"Error opening file\n");
	}
	fscanf(f_charge,"%ju",&charge_now);
	
	// Read Current_now
	int current_now;
	FILE *f_current = fopen(POWER_SUPPLY_CURRENT,"r");
	if (f_current == NULL) { fprintf(stderr,"Error opening file\n");
	}
	fscanf(f_current,"%ju",&current_now);

	double timeleft = (double)charge_now / (double)current_now;
	int h = timeleft;
	int m = (timeleft - (double)h) * 60;
	printf(" Time remaining %d:%d min",h,m);
}


int main() {
    printf("Battery capacity: %d \n",battery_percent());
    //printf("Battery remaining: %d\n",
    battery_remaining();
    return 0;
}

#include <stdio.h>

#define POWER_SUPPLY_CAPACITY "/sys/class/power_supply/BAT1/capacity"
#define POWER_SUPPLY_STATUS "/sys/class/power_supply/BAT1/status"
#define POWER_SUPPLY_CHARGE "/sys/class/power_supply/BAT1/charge_now"
#define POWER_SUPPLY_CURRENT "/sys/class/power_supply/BAT1/current_now"

const int get_battery_info(const char* filename) {
	int tmp = 0;
	FILE* fd = fopen(filename,"r");

	if(fd == NULL) {
		fprintf(stderr,"Error opening file: %s\n",
				filename);
	}
	fscanf(fd,"%d",&tmp);
	fclose(fd);
	return tmp;
}

const int battery_percent() {
	int battery_percent;
	battery_percent = get_battery_info(POWER_SUPPLY_CAPACITY);
	printf("%d%% ", battery_percent);
}

const int battery_remaining() {
	// Read charge_now
	int charge_now;
	charge_now = get_battery_info(POWER_SUPPLY_CHARGE);
	
	// Read Current_now
	int current_now;
	current_now = get_battery_info(POWER_SUPPLY_CURRENT);

	double timeleft = (double)charge_now / (double)current_now;
	int h = timeleft, m = (timeleft - (double)h) * 60;
	printf("%d:%02d min",h,m);
}

int main() {
    battery_percent();
    battery_remaining();
    return 0;
}

#include <stdio.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "colormap.h"
#include "conf.h"

void alloc_cmap(struct fb_cmap *cmap)
{
    // Alloc colormap
    cmap->red = malloc(16 * sizeof(uint16_t));
    cmap->green = malloc(16 * sizeof(uint16_t));
    cmap->blue = malloc(16 * sizeof(uint16_t));
    cmap->transp = 0;
    cmap->start = 0;
    cmap->len = 16;
}

void release_cmap(struct fb_cmap *cmap)
{
    free(cmap->red);
    free(cmap->green);
    free(cmap->blue);
    free(cmap);
}

void parse_colormap(struct color_array *ca)
{
    unsigned int r;
    unsigned int g;
    unsigned int b;

    for(int i = 0; i < COLOR_LENGTH; i++) {
        sscanf(colorname[i],"#%2x%2x%2x",&r,&g,&b);

        ca->red[i] = r;
        ca->green[i] = g;
        ca->blue[i] = b;
    }
}

int set_cmap(struct framebuffer *fb,struct fb_cmap *cmap,struct color_array *ca)
{
    unsigned short r[256];
    unsigned short b[256];
    unsigned short g[256];

    // Set colormap
    cmap->start = 0;
    cmap->len = 16;
    cmap->red = r ;
    cmap->green = g;
    cmap->blue = b;
    cmap->transp = 0;

    for(int i = 0 ; i < cmap->len; i++) {
        r[i] = ca->red[i] << 8;
        g[i] = ca->green[i] << 8;
        b[i] = ca->blue[i] << 8;
    }
    if(ioctl(fb->fd,FBIOPUTCMAP,cmap) == -1) {
        printf("[colormap.c:set_cmap] Error FBIOPUTCMAP %s\n",strerror(errno));
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

// Framebuffer Font rendering using Freetype2
// gcc -g -Wall -std=c11 sfbII.c -I/usr/include/freetype2 -lfreetype -o SFB
//

#include <stdio.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <ft2build.h>

#include FT_FREETYPE_H
#define max(a,b) ((a) > (b) ? (a) : (b))


// ----------- [ FRAMEBUFFER ] -----------
struct framebuffer_t {
	int fd;
	int width;
	int height;
	int bytes;
	int data_size;
	int line_length;
	int stride;
	int slop;
	
	char* data;

};
// PROTO 
void draw_char(FT_Face face, struct framebuffer_t *, char , int *, int );
void draw_str(FT_Face face, struct framebuffer_t *,const char *, int *, int);

void init_framebuffer(struct framebuffer_t *fb) {
	fb->fd = open("/dev/fb0",O_RDWR);

	struct fb_fix_screeninfo finfo;
	struct fb_var_screeninfo vinfo;

	ioctl(fb->fd,FBIOGET_FSCREENINFO,&finfo);
	ioctl(fb->fd,FBIOGET_VSCREENINFO,&vinfo);

	fb->width = vinfo.xres;
	fb->height = vinfo.yres;
	// vinfo.bits_per_pixel = 32, each pixel has 4bytes in size 
	fb->bytes = vinfo.bits_per_pixel / 8;

	// Calculate the amount of memory it occupies
	fb->data_size = fb->width * fb->height * fb->bytes;
	fb->stride = max(finfo.line_length , fb->width * fb->bytes);
	fb->slop = fb->stride - (fb->width * fb->bytes);
	
	fb->data = mmap(0, fb->data_size,
		PROT_READ | PROT_WRITE, MAP_SHARED,
		fb->fd, (off_t)0);
	
	// Read or write the mapped Screen
	// Blank the entire screen set the area to 0
	memset(fb->data, 0, fb->data_size);
}

void init_font(struct framebuffer_t *fb) {
	//Opening Font
	FT_Library ft;
	if(FT_Init_FreeType(&ft))
		fprintf(stderr,"Error: Library initialization failed\n");


	// Creating a type face 
	FT_Face face;
	if(FT_New_Face(ft,"/home/crsohe/.local/share/fonts/Code New Roman Nerd Font Complete.otf",0,&face))
			fprintf(stderr,"Error: Loading font file\n");

	int font_size = 14;	
	if(FT_Set_Pixel_Sizes(face, 0, font_size)) 
		fprintf(stderr, "Error: Set_pixel_sizes\n");
	
	// Drawing the bitmap
	int x = 600;
	int y = 100;

	draw_str(face,fb,"Hello CBTech,Framebuffer is awesome ",&x,y);
	// Close the font 
	FT_Done_FreeType(ft);
}

static void framebuffer_set_pixel(struct framebuffer_t *fb,int x, int y,int w, int h,unsigned char r,
		unsigned char  g,unsigned char b){
	if(x > 0 && x < w && y > 0  && y < h) {
		int index32 = (y * w + x) * fb->bytes + y * fb->slop;
	fb->data[index32++] = b;
	fb->data[index32++] = g;
	fb->data[index32++] = r;
	fb->data[index32++] = 0;
	}
}

void draw_char(FT_Face face, struct framebuffer_t *fb, char c, int *x, int y) {
	FT_UInt gi = FT_Get_Char_Index(face,c);
	FT_Load_Glyph(face, gi, FT_LOAD_DEFAULT);

	// Define metrics
	int bbox_ymax = face->bbox.yMax / 64;
	int glyph_width = face->glyph->metrics.width / 64;
	int advance = face->glyph->metrics.horiAdvance / 64;
	int x_off = (advance - glyph_width) / 2;
	int y_off = bbox_ymax - face->glyph->metrics.horiBearingY / 64;

	// Render the glyph into memory
	FT_Render_Glyph(face->glyph,FT_RENDER_MODE_NORMAL);	

	for(int i = 0; i < (int)face->glyph->bitmap.rows; i++) {
		//row_offset is the distance from the top of the framebuffer
		// of the text bounding box
		int row_offset = y + i + y_off;
		for(int j = 0; j < (int)face->glyph->bitmap.width; j++) {
			unsigned char p = face->glyph->bitmap.buffer[i * face->glyph->bitmap.pitch + j];

			// Don't draw a zero value, unless you want to fill the bounding box with black
		if(p)
			framebuffer_set_pixel(fb,*x + j+ x_off, row_offset,fb->width, 
					fb->height, p, p, p);
		}
	}
	// Move the x position, ready for the next character.
	*x += advance; 
}

void draw_str(FT_Face face, struct framebuffer_t* fb,const char *str, int *x, int y) {
	while(*str) {
		draw_char(face, fb, *str, x, y);
		str++;
	}
}

// ----------- [ TERMINAL ] ----------
struct term_t {
	int width;
	int height;
	int cols;
	int line;
//	uint32_t color_palete
};


void init_term() {
	struct termios termios;
	tcgetattr(0,&termios);
	cfmakeraw(&termios);

}

void term_exec() { 
}

int main() {
	struct framebuffer_t *fb;
	fb = malloc(sizeof(struct framebuffer_t));

	struct term_t *term;
	
	init_framebuffer(fb);
	init_font(fb);
	// Clean up the framebuffer
	munmap (fb->data, fb->data_size);
	close(fb->fd);
	free(fb);
}

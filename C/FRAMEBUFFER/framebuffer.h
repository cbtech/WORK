#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <linux/fb.h>

struct color_array;
struct pseudo_term;

struct framebuffer {
	int fd;
	int width;
	int height;
	int bytes;
	int data_size;
	int line_length;
	int stride;
	int slop;
	
	char* data;

};

// PROTO
void init_font(const char *path);
void init_framebuffer(struct framebuffer *);
void set_background(struct framebuffer *,struct fb_cmap *,struct color_array * );
void output_to_fb(struct framebuffer *,struct pseudo_term *);

#endif

// Framebuffer Font rendering using Freetype2
// gcc -g -Wall -std=c11 sfbII.c -I/usr/include/freetype2 -lfreetype -o SFB
//

#define _XOPEN_SOURCE 600

#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/select.h>
#include <linux/fb.h>
#include <stdlib.h>
#include <sys/wait.h> // wait_pid
#include <linux/ioctl.h>
#include <linux/vt.h>   // VT_SETMODE / vt_mode
#include <signal.h>
#include <errno.h>

#include "colormap.h"
#include "framebuffer.h"
#include "pty.h"

static struct framebuffer fb;
static struct fb_cmap cmap;
static struct color_array c_array;
static struct pseudo_term pty;

void main_loop(struct framebuffer *fb,struct fb_cmap *cmap,
        struct color_array *ca,void* buffer)
{
   if(set_cmap(fb,cmap,ca) == -1) {
       fprintf(stderr,"[sfb.c:main_loop]Error set_cmap\n");
       exit(EXIT_FAILURE);
   }

   output_to_fb(fb,&pty);
   // draw cursor
}

static void signal_received(int n)
{
    switch (n) {
    case SIGUSR1:
        if(ioctl(0, VT_RELDISP, 1) == -1) {
            fprintf(stderr,"[sfb.c:signalReceived] ioctl VT_RELDISP failed\n");
            exit(EXIT_FAILURE);
        }
        break;
    case SIGUSR2:
        main_loop(&fb,&cmap,&c_array,&pty.buffer);
        break;
    }
}

static void signal_setup(void)
{
    struct vt_mode vtm;
    vtm.mode = VT_PROCESS;
    vtm.waitv = 0;
    // Associate user defined signal SIGUSR1 to vtm.relsign
    // to release request and SIGUSR2 to vtm.acqsig to acquire request
    vtm.relsig = SIGUSR1;
    vtm.acqsig = SIGUSR2;
    vtm.frsig = 0;
    
    if(signal(SIGUSR1, signal_received) == SIG_ERR ||
       signal(SIGUSR2, signal_received) == SIG_ERR) {
        fprintf(stderr,"[sfb.c:signalSetup]Error signal\n");
        exit(EXIT_FAILURE);
    }
    
    if(ioctl(0, VT_SETMODE, &vtm) == -1) 
        fprintf(stderr,"[sfb.c:signalSetup]Error ioctl signal\n");
}

int main(int argc,char **argv)
{
    init_framebuffer(&fb);
    init(&pty,&fb);
    init_font("/home/cbtech/.local/share/fonts/JetBrains Mono Regular Nerd Font Complete.ttf");
    size_t n_read;

    // Create a child process that is connected to
    // the parent using pseudoterminal_pair
    set_terminal(&pty);
    exec_term(&pty);

    signal_setup();

    // Colormap
    alloc_cmap(&cmap);
    parse_colormap(&c_array);
    set_cmap(&fb,&cmap,&c_array);

    set_background(&fb,&cmap,&c_array);
    for(;;) {
        // macro removes all fd from set
        fd_set fds;
        FD_ZERO(&fds);
        // select is used to asynchronous call between this two fd
        // macro adds the fd to set
        FD_SET(STDIN_FILENO,&fds);
        FD_SET(pty.master_fd,&fds);

        if(select(pty.master_fd +1, &fds,
                  NULL, NULL, NULL) == -1)
            printf("[sfb.c:main] Error select %s\n",strerror(errno));


        if(FD_ISSET(STDIN_FILENO,&fds)) {
            n_read = read(STDIN_FILENO,pty.buffer,BUFFER_SIZE);
            if(write(pty.master_fd,pty.buffer, n_read) != n_read)
                printf("[sfb.c:main] Failed to write STDIN_FILENO)\n");
        }

        if(FD_ISSET(pty.master_fd,&fds) ) {
        n_read = read(pty.master_fd,pty.buffer,BUFFER_SIZE);
       }

    main_loop(&fb,&cmap,&c_array,&pty.buffer);
    }

    // Close properly
    release_and_close(&pty);
    return 0;
}

#ifndef PTY_H
#define PTY_H

struct framebuffer;
#define BUFFER_SIZE 655635

struct pseudo_term {
	int master_fd;
	int slave_fd;
	int perm;
	char *slave_name;
	int rows;
	int cols;
        char *buffer;
};

void init(struct pseudo_term *,struct framebuffer *);
void set_terminal(struct pseudo_term *); 
pid_t spawn_pty(struct pseudo_term *);
int exec_term(struct pseudo_term *);
void release_and_close(struct pseudo_term *);
#endif

#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <ft2build.h>
#include <sys/mman.h>
#include <errno.h>
#include <stdint.h>
#include <linux/fb.h>
#include <ctype.h>
#include "framebuffer.h"
#include "colormap.h"
#include "pty.h"

#include FT_FREETYPE_H

#define MAX(a, b)	((a) > (b) ? (a) : (b))

struct fb_var_screeninfo vinfo;
struct fb_fix_screeninfo finfo;

#define WIDTH 1920
#define HEIGHT 1079

static FT_Face font_face;
static FT_Library ft;

void init_font(const char *path) {
	//Opening Font
	if(FT_Init_FreeType(&ft))
		fprintf(stderr,"[framebuffer.c:init_font] Library initialization failed\n");

	// Creating a type face 
	if(FT_New_Face(ft,path,0,&font_face))
			fprintf(stderr,"[framebuffer.c:init_font] Loading font failed\n");

	int font_size = 14;	
	if(FT_Set_Pixel_Sizes(font_face, 0, font_size)) 
		fprintf(stderr, "[framebuffer.c:init_font] Set font sizes failed\n");
}

void release_font() {
        // Close the font 
	FT_Done_FreeType(ft);
}

void init_framebuffer(struct framebuffer *fb)
{
    fb->fd = open("/dev/fb0",O_RDWR);

    ioctl(fb->fd,FBIOGET_FSCREENINFO,&finfo);
    ioctl(fb->fd,FBIOGET_VSCREENINFO,&vinfo);

    fb->width = vinfo.xres;
    fb->height = vinfo.yres;
    // vinfo.bits_per_pixel = 32, each pixel has 4bytes in size
    fb->bytes = vinfo.bits_per_pixel / 8;

    // Calculate the amount of memory it occupies
    fb->data_size = fb->width * fb->height * fb->bytes;
    fb->stride = MAX(finfo.line_length, fb->width * fb->bytes);
    fb->slop = fb->stride - (fb->width * fb->bytes);

    fb->data = mmap(0, fb->data_size,
                    PROT_READ | PROT_WRITE, MAP_SHARED,
                    fb->fd, (off_t)0);
}

void set_background(struct framebuffer *fb,struct fb_cmap *cmap,
        struct color_array *ca) {
        int x,y;    
        memset(fb->data, 55,fb->data_size); 
}

static void draw_pixel(struct framebuffer *fb,int x, int y,int w, int h,unsigned char r,
		unsigned char  g,unsigned char b){
	if(x > 0 && x < w && y > 0  && y < h) {
		int offset = (y * w + x) * fb->bytes + y * fb->slop;
	fb->data[offset++] = b;
	fb->data[offset++] = g;
	fb->data[offset++] = r;
	fb->data[offset++] = 0;
        }
}

void draw_char(FT_Face face, struct framebuffer *fb, char c, int *x, int y) {
	FT_UInt gi = FT_Get_Char_Index(face,c);
	FT_Load_Glyph(face, gi, FT_LOAD_DEFAULT);

	// Define metrics
	int bbox_ymax = face->bbox.yMax / 64;
	int glyph_width = face->glyph->metrics.width / 64;
	int advance = face->glyph->metrics.horiAdvance / 64;
	int x_off = (advance - glyph_width) / 2;
	int y_off = bbox_ymax - face->glyph->metrics.horiBearingY / 64;

	// Render the glyph into memory
	FT_Render_Glyph(face->glyph,FT_RENDER_MODE_NORMAL);	

	for(int i = 0; i < (int)face->glyph->bitmap.rows; i++) {
		//row_offset is the distance from the top of the framebuffer
		// of the text bounding box
		int row_offset = y + i + y_off;
		for(int j = 0; j < (int)face->glyph->bitmap.width; j++) {
			unsigned char p = face->glyph->bitmap.buffer[i * face->glyph->bitmap.pitch + j];

			// Don't draw a zero value, unless you want to fill the bounding box with black
		if(p)
			draw_pixel(fb,*x + j+ x_off, row_offset,fb->width, 
					fb->height, p, p, p);
		}
	}
	// Move the x position, ready for the next character.
	*x += advance; 

}
static void draw_str(FT_Face face, struct framebuffer* fb,const char *str, int *x, int y) {
	while(*str) {
		draw_char(face, fb, *str, x, y);
		str++;
	}
}

static int pos_x = 10;
static int pos_y = 10;

void output_to_fb(struct framebuffer *fb,struct pseudo_term *pts) {
	// Drawing the bitmap
        for ( ;*pts->buffer != '\0'; pts->buffer++) {
	        draw_char(font_face,fb,*pts->buffer,&pos_x,pos_y);

            if(*pts->buffer == '\n') {pos_x =10; pos_y += 20;}
            if (pos_x <= fb->width - 80) pos_x++;

            else { pos_x = 10;  pos_y += 20; }
        }
}


// Simple Pseudo terminal utilisation in C
// Don't render anything, just create a process using fork
// Don't forget to put the define below in first inclusion

#define _XOPEN_SOURCE 600

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <sys/select.h>

#include "pty.h"
#include "framebuffer.h"

#define COLS 8
#define LINES 16

void init(struct pseudo_term *pty,struct framebuffer *fb)
{
    pty->rows = fb->height / LINES;
    pty->cols = fb->width / COLS;
    pty->buffer = malloc( BUFFER_SIZE * sizeof(char));
}

int open_master(struct pseudo_term *pty)
{
    if ((pty->master_fd = open("/dev/ptmx",O_RDWR | O_NOCTTY)) == -1
        || grantpt(pty->master_fd) == -1
        || unlockpt(pty->master_fd) == -1
        || (pty->slave_name = ptsname(pty->master_fd)) == NULL) {
        fprintf(stderr,"[pty.c:open_master] Cannot open master\n");
        return -1;
    }
    return 0;
}

void set_terminal(struct pseudo_term *pty) {
    struct termios slave_termios;
    struct winsize *ws;

    memset (&ws,0,sizeof(ws));

    // Get terminal attributes
    if(tcgetattr(STDIN_FILENO,&slave_termios) <0)
        fprintf(stderr,"[pty.c:set_terminal] Cannot get terminal attributes\n");

    slave_termios.c_lflag  &= ~(ICANON | ISIG | ECHO );
    slave_termios.c_cc[VMIN] = 1;
    slave_termios.c_cc[VTIME] = 0;
    // Set Terminal attributes
    if(tcsetattr(pty->slave_fd,TCSANOW,&slave_termios) == -1)
       fprintf(stderr,"[pty.c:set_terminal] Setting term\n");

    if(ws != NULL)  {
        if(ioctl(pty->slave_fd, TIOCSWINSZ, ws) == -1)
            fprintf(stderr,"[pty.c:set_terminal]Error setting winsize\n");
    }
}

pid_t spawn_pty(struct pseudo_term *pts)
{
    open_master(pts);

    pid_t child_pid = fork();
    if(child_pid == -1) { // ERROR
        fprintf(stderr,"[pty.c spawn_pty] Cannot fork\n");
        exit(EXIT_FAILURE);
    }
    else if(child_pid != 0) {
        return EXIT_FAILURE;
    }

    if(setsid() < 0)
        fprintf(stderr,"[pty.c:spawn_pty] Cannot start session setsid\n");

    pts->slave_fd = open(pts->slave_name,O_RDWR);

    if(dup2(pts->slave_fd, STDIN_FILENO) != STDIN_FILENO ||
       dup2(pts->slave_fd, STDOUT_FILENO) != STDOUT_FILENO ||
       dup2(pts->slave_fd, STDERR_FILENO) != STDERR_FILENO) {
       fprintf(stderr,"[pty.c:spawn_pty] Cannot duplicate slave\n");
    }
    return 0;
}

const char *shell_cmd="/bin/bash";
int exec_term(struct pseudo_term* pts)
{
    pid_t pid;
    pid = spawn_pty(pts);

    if(pid < 0) {           // ERROR
        return 0;
    } else if(pid == 0) {   // Child
        execl(shell_cmd,shell_cmd,(char*)NULL);
        exit(EXIT_FAILURE);
    }
    return 1;
}

void release_and_close(struct pseudo_term *pts)
{
    close(pts->master_fd);
    close(pts->slave_fd);
    free(pts);
}

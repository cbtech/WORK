#ifndef CONF_H
#define CONF_H

#define COLOR_LENGTH 16

//MONOKAI THEME
static const char *colorname[] = {
    "#000000",
    "#FF0000",
    "#0000FF",
    "#FFFC00",
    "#0000FF",
    "#FF00E9",
    "#00FFE0",
    "#FFFFFF",
    "#000000",
    "#FF0000",
    "#0000FF",
    "#FFFC00",
    "#0000FF",
    "#FF00E9",
    "#00FFE0",
    "#FFFFFF"
};
#endif

#ifndef COLORMAP_H
#define COLORMAP_H

#include <stdint.h>
#include "framebuffer.h"
#include "conf.h"

struct framebuffer_t;
struct color_array {
    uint16_t red[COLOR_LENGTH];
    uint16_t green[COLOR_LENGTH];
    uint16_t blue[COLOR_LENGTH];
};

void alloc_cmap(struct fb_cmap *);
void cmap_init(struct fb_cmap *);
void release_cmap(struct fb_cmap *);
void parse_colormap(struct color_array *);
int set_cmap(struct framebuffer *,struct fb_cmap *,struct color_array *);

#endif

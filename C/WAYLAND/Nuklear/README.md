
# minimal_NKWin Minimal Wayland window
 1. Create connection with compositor using wayland Display → line29 
 2. Bind compositor using registry listener(handle_global function) → line 17 to 31
    We use registry to get interface about our compositor and initialize our compositor
    var to avoid segmentation fault.
 3. Create a surface and a xdg_surface and add a listener to initialize it.
    Bind xdg_wm_base in handle_global
 4. Add xdg_toplevel to defines simple application (xdg_popup is for dopdown menu,or tooltip)
 5. Create buffer and pool(initialize in handle_globale wl_shm) 
     create shm file and file descriptor



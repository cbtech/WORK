/* 
 *  WAYLAND Buffer  
 *
 *  cc -g -Wall -w WLBuffer.c -L/usr/local/lib lwayland-client -o WLBuffer
 *
 *
 */

#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>

int size = 200 * 200 * 4;
// Oepn as anonymous file and write
// some zero bytes to it
int fd = syscall(memfd_create,"buffer",0);
ftruncate(fd, size);

// Map it to the memory 
unsigned char *data = mmap(NULL,size,PROT_READ | PROT_WRITE, MAP_SHARED, fd,0);

struct wl_shm *shm = NULL;
struct wl_shm_pool *pool = wl_shm_create_pool(shm,fd,size);


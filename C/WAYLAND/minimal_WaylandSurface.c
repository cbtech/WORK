// Minimal Wayland Window 
// wayland-scanner private-code < /usr/share/wayland-protocols/stable/xdg-shell/xdg-shell.xml > xdg-shell.c
// wayland-scanner private-code < /usr/share/wayland-protocols/stable/xdg-shell/xdg-shell.xml > xdg-shell.c
// gcc -g -Wall minimal_NKWin.c xdg_shell.c -l wayland-client -o NK_minimalWin

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <wayland-client.h>
#include <sys/mman.h>  //PROT_READ | PROT_WRITE
#include "xdg-shell.h"

#define WIDTH 400
#define HEIGHT 200

static int running = 1;
static void noop(){}
static struct wl_display *display = NULL;
static struct wl_registry *registry = NULL;
static struct wl_surface *surface = NULL;
static struct wl_compositor *compositor = NULL;
static struct wl_buffer *buffer = NULL;
static struct wl_shm *shm = NULL;

// XDG STRUCT
static struct xdg_wm_base *xdg_wm_base = NULL;
static struct xdg_toplevel *xdg_toplevel = NULL;

static void handle_global(void *data, struct wl_registry *registry,
		uint32_t name, const char* interface,uint32_t version) {
	if(strcmp(interface,wl_compositor_interface.name) == 0) {
		compositor = wl_registry_bind(registry, name, &wl_compositor_interface, 1);
	} else if(strcmp(interface,xdg_wm_base_interface.name) == 0) {
		xdg_wm_base = wl_registry_bind(registry, name, &xdg_wm_base_interface, 1);
	} else if(strcmp(interface,wl_shm_interface.name) == 0) {
		shm = wl_registry_bind(registry, name,&wl_shm_interface, 1);
	}
}

static void handle_global_remove(void *data,struct wl_registry *registry,uint32_t name) {
}

static const struct wl_registry_listener registry_listener = {
	.global = handle_global,
	.global_remove = handle_global_remove
};

static void xdg_surface_handle_configure(void *data,
		struct xdg_surface *xdg_surface, uint32_t serial) {
	xdg_surface_ack_configure(xdg_surface,serial);
	wl_surface_commit(surface);
}

static const struct xdg_surface_listener xdg_surface_listener = {
	.configure = xdg_surface_handle_configure,
};

static void xdg_toplevel_handle_close() {
	running = 0;
}

static const struct xdg_toplevel_listener xdg_toplevel_listener = {
	.configure = noop,
	.close = xdg_toplevel_handle_close,
};


int main() {
	
	// Information about wayland display is located
	// in /usr/include/wayland/wayland-client-core.h
	display = wl_display_connect(NULL);
	if(display == NULL) 
		fprintf(stderr,"Error opening display\n");

	registry = wl_display_get_registry(display);
	wl_registry_add_listener(registry, &registry_listener, NULL);	
	wl_display_roundtrip(display);
	
	// CREATE POOL & BUFFER
	size_t size = WIDTH * HEIGHT * 4;
	int fd = allocate_shm_file(size);
	uint8_t *pool_data = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	struct wl_shm_pool *pool = wl_shm_create_pool(shm,fd,size);
	buffer = wl_shm_pool_create_buffer(pool, 0, WIDTH, HEIGHT, WIDTH * 4, WL_SHM_FORMAT_ARGB8888);
	wl_shm_pool_destroy(pool);

	// Create surface
	surface = wl_compositor_create_surface(compositor); 		
	struct xdg_surface *xdg_surface = xdg_wm_base_get_xdg_surface(xdg_wm_base,surface);
	xdg_toplevel = xdg_surface_get_toplevel (xdg_surface);
	xdg_surface_add_listener(xdg_surface,&xdg_surface_listener, NULL);
	xdg_toplevel_add_listener(xdg_toplevel,&xdg_toplevel_listener, NULL);
	
	wl_surface_commit(surface);
	wl_display_roundtrip(display);

	
	wl_surface_attach(surface, buffer, 0, 0);
	wl_surface_commit(surface);
	
	// Infinite loop
	while(wl_display_dispatch(display) != -1 && running) {
	}

	xdg_toplevel_destroy(xdg_toplevel);
	xdg_surface_destroy(xdg_surface);
	wl_surface_destroy(surface);	
	return 0;

}

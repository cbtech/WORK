/* SIMPLE WAYLAND SERVER
 cc -g -Wall -w -I/usr/local/include WLServer.c -L/usr/local/lib -lwayland-server
 Adding Globals(interfaces)  
*/
#include <stdio.h>
#include <wayland-server.h>

static void wl_output_handle_resource_destroy(struct wl_resource *resource)
{
	struct my_output *client_output = wl_resource_get_user_data(resource);
	// TODO: Clean up resources
	remove_to_list(client->output->state->client_outputs, client_output);
}

static void wl_output_resource_destroy(struct wl_resource *resource)
{
	wlr_resource_destroy(resource);
}


int main(int argc,char *argv[])
{
	struct wl_display *display = wl_display_create();

	if(!display) {
		fprintf(stderr,"Unable to create Wayland display\n");
		return 1;
	}

	const char *socket = wl_display_add_socket_auto(display);
	if(!socket)  {
		fprintf(stderr, "Unable to add socket to Wayland display\n");
		return 1;
	}

	fprintf(stderr,"Running Wayland display on %s\n", socket);
	wl_display_run(display);

	wl_display_destroy(display);
	return 0;
}

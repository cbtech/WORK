#include <stdio.h>
#include <string.h>
#include <wayland-client.h>
#include <wayland-egl.h>
#include "wlr-layer-shell-unstable-v1-protocol.h"

struct layer_shell {
	struct wl_compositor *compositor;
	struct wl_display *display;
	struct wl_registry *registry;
	struct zwlr_layer_shell_v1 *shell;
	struct wl_surface *surface;
	struct zwlr_layersurface_v1 *layer_surface;
};

static void registry_handle_global(void *data,struct wl_registry *registry,uint32_t id,
		const char *interface, uint32_t version) {
	struct layer_shell *shell = data;

	if(strcmp(interface,"zwlr_layer_shell_v1") == 0) {
			shell->shell = wl_registry_bind(registry,id, &zwlr_layer_shell_v1, 1);
			}
}

static const struct wl_registry_listener registry_listener = {
	registry_handle_global
};

int main() {
	struct layer_shell shell;

	shell.display = wl_display_connect(NULL);
	if(!shell.display) {
		return -1;
	}

	shell.registry = wl_display_get_registry(shell.display);
	wl_registry_add_listener(shell.registry, &registry_listener, &shell);
	wl_display_dispatch(shell.display);

	shell.surface = wl_compositor_create_surface(shell.compositor);
	shell.layer_surface = zwlr_layer_shell_v1_get_layer_surface(shell.shell, shell.surface, NULL, ZWLR_LAYER_SHELL_V1_LAYER_TOP, "Example");

	wl_display_disconnect(shell.display);

}

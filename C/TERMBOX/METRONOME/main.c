// SIMPLE CLI METRONOME USING TERMBOX
// gcc -g main.c -L. -ltermbox -o METRONOME

#include <stdio.h>
#include "termbox.h"

void draw_interface() {

	// Draw title
	tb_printf(30, 0, TB_WHITE, TB_CYAN,"[CLI Metronome]");
	// Draw  Left Button
	const int BTN1_POS_X = 5;
	const int BTN1_POS_Y = 5;
	for(int x =0, y = 0; x <= 2; x++,y++) {
	  tb_set_cell( BTN1_POS_X + x, BTN1_POS_Y ,0x2588,
		       TB_BLUE,TB_CYAN);
	  tb_set_cell( BTN1_POS_X + x, BTN1_POS_Y + 1 ,
		     0x2588,TB_BLUE,TB_CYAN);
	}
	// Draw  Right Button
	const int BTN2_POS_X = 65;
	const int BTN2_POS_Y = 5;
	for(int x =0, y = 0; x <= 2; x++,y++) {
	  tb_set_cell( BTN2_POS_X + x, BTN2_POS_Y ,0x2588,
		       TB_BLUE,TB_CYAN);
	  tb_set_cell( BTN2_POS_X + x, BTN2_POS_Y + 1 ,
		     0x2588,TB_BLUE,TB_CYAN);
	}

	tb_present();
}

int main(int argc, char **argv) {
	int ret = tb_init();
	if(ret) {
		fprintf(stderr,"Error Initializing termbox\n");
	}
	char *tmuxenv = getenv("TMUX");
	if (tmuxenv) {
		fprintf(stderr,"Close TMUX. Thanks\n");
		tb_shutdown();
		exit(1);
	}
	printf("TMUX ENV: %s\n",tmuxenv);
	struct tb_event ev;

	draw_interface();

	while (tb_poll_event(&ev) == TB_OK) {
           switch (ev.type) {
       		 case TB_EVENT_KEY:
          	  if (ev.key == TB_KEY_CTRL_Q) {
                	tb_shutdown();
                  return 0;
            	}
		break;

  		case TB_EVENT_RESIZE:
            		tb_clear();
			draw_interface();
            	break;
 	   }
		}
	tb_shutdown();
	return 0;

}

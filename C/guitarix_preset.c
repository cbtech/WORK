// Guitar preset using RPC communication
// gcc -g -Wall -std=c11 guitarix_preset.c -o ~/EXEC/GUITARIX_PRESET
//

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

static const char *SERVER = "/usr/bin/guitarix";
static const char *args[] = { "-N", "-p 7000 ","-i system:capture_1",
                              "-o system:playback_1", "-o system:playback_2"};

int main(int argc,char *argv[]) {

  if(execl(SERVER,*args,NULL) == -1) 
    printf("Error opening server %s",strerror(errno));

  return 0;

}

// Daft Scraper using Curl ( Start: July 2022)
// tcc  -g -Wall -w daftscrape.c -lcurl -o DAFTSCRAPE
// DEBIAN
// tcc -g -Wall daftscrape.c -I/usr/include/x86_64-linux-gnu -lcurl -o DAFTSCRAPE

#include <curl/curl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define FALSE 0
#define TRUE 1

struct buffer_t{
	char* content;
	size_t size;
};

struct scrapper_t {
	char *title;
	char *id_1;
	char *id_2;
};

size_t grow_buffer(void *content,size_t size,size_t nb_elems,void *ctx)
{
	size_t real_size = size * nb_elems;
	struct buffer_t *buffer = (struct buffer_t*) ctx;
	char *ptr = realloc(buffer->content, size + real_size);
	if(ptr)  {
		buffer->content = ptr;
		memcpy(&(buffer->content[buffer->size]),content,real_size);
		buffer->size += real_size;
	}

	else {
		fprintf(stderr,"Not enough memory(Realloc failed)\n");
		return 0;
	}

	return real_size;
}

// extract to start and calcul the length 
const char *extract_str(const char *src,char *dst,int start,int end) {
	int j = 0;

	for (int i = start; i <= end ;i++,j++) {
		dst[j] = src[i];
	}

//	dst[j] = '\0';
	// DOESN'T WORK
	// Ok for the start but the end is not correct
	//for(src += start; (*dst = *src) && src != end; src++,dst++) ;
	return dst;
}

void _strlen(const char*str) {
	int i = 0;
	for ( ; *str !='\0'; str++) { i++; }
}

void extract_title_tag(struct buffer_t *buffer,char *string,struct scrapper_t *s)
{
	int start = 0,end = 0,j = 0;
	int occurence_length = strlen(string);
	int occurence_found = FALSE;
	int matches = 0, position = 0;

	// Loop through the buffer
	for(char *c = buffer->content; *c != '\0' ;c++) {
		if(*c == '<' && occurence_found) {
			// End of occurence
			end = position - 1;
			occurence_found = FALSE;
		}

		if (*c == string[j]) {
			// Check if all letters is in occurrence 
			for (int i = 0;i <= occurence_length; i++)
			{
				if(c[i] == string[i]) {
					matches++;
					// If the two occurences length are equal
					if (matches == occurence_length) {
						start = position + occurence_length;
						occurence_found = TRUE;
					}
				}
					else { 
						// That doesn't match
						matches = 0;
					}
			}
		}
		position++;
	}

	// Dynamic allocation for Title 
	s->title = malloc((end - start) +1);
	extract_str(buffer->content,s->title,start,end);
}


void extract_id(const char *url,struct scrapper_t *scrapper) {
	// Extract the first and second ID
	const char *pattern = "https://daft.sex/watch/[ID]";	
	char delimiter = '_';
	const int START = 0;
	int position,length = 0;
	int first_id_extracted = 0, second_id_extracted = 0;

	for( ;*url !='\0'; url++,pattern++)
	{
		if(*url != *pattern) {
			// Extract first ID 
			if(!first_id_extracted) {
				for(int i = 0;url[i] != delimiter; i++) {
					length = i;
				}
				scrapper->id_1 = malloc(length+1);
				extract_str(url,scrapper->id_1,START,length);
				url += length + 1;
				first_id_extracted = TRUE;
			}
			// Extract second ID
			else if (!second_id_extracted) {
				if (*url == delimiter) url++;
				// Second ID has the same length
				scrapper->id_2 = malloc(length+1);
				extract_str(url,scrapper->id_2,START,length);
				second_id_extracted = TRUE;
			}

		}
		position++;
	}
}

void free_res(struct buffer_t *buffer,struct scrapper_t *scrapper) {

	
	free(buffer->content);
	free(buffer);
	free(scrapper->title);
	free(scrapper->id_1);
	free(scrapper->id_2);
}
int pipe_st=0;
CURLcode easy_perform(struct Curl_easy *data) {
	CURLMcode mcode;

  	CURLcode result = CURLE_OK;
 	 SIGPIPE_VARIABLE(pipe_st);

  if(!data)
    return CURLE_BAD_FUNCTION_ARGUMENT;

}

int main(int argc,char **argv)
{

	const char *url = "https://daft.sex/watch/-23078988_456239268";
	CURL *handle = curl_easy_init();
	CURLcode res;


	// GET FIRST ARG
	//const char *url = argv[1];
	struct scrapper_t scrapper;

	struct buffer_t *buffer = malloc(sizeof(struct buffer_t));

	buffer->content = malloc(1);
	buffer->size = 0;

	if(handle) {
		curl_easy_setopt(handle,CURLOPT_URL,url);
		// READ THE CONTENT TO TTY
		//READ THE CONTENT AND EXPORT IT WHEREVER YOU WANT
		curl_easy_setopt(handle,CURLOPT_WRITEFUNCTION,grow_buffer);
		curl_easy_setopt(handle,CURLOPT_WRITEDATA,buffer);
		curl_easy_setopt(handle, CURLOPT_PRIVATE, buffer);
		
		res = curl_easy_perform(handle);

		// PRINT BUFFER CONTENT
		extract_title_tag(buffer,"<title>",&scrapper);
		printf("Title: %s\n", scrapper.title);
		extract_id(url,&scrapper);
		printf("ID1: %s\n", scrapper.id_1);
		printf("ID2: %s\n",scrapper.id_2);
	
		}

	// Free All resources
	free_res(buffer,&scrapper);
	return 0;
}

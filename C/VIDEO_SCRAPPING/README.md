# [ISSUES]
	malloc issues on line 176 → curl_easy_setopt.	
	malloc(): invalid size(unsorted).

	curl_easy_perform is located in curl/lib/easy.c l766 
	it returns CURL_Code using easy_perform function
	Rewrite function curl_easy_setopt to debug 
# SOURCE
	CURL Example:
		getinmemory.c
		crawler.c
		postinmemory.c
		http2-pushinmemory.c 
# [TODO]
	You can use dynamic memory allocation in struct scrapper
	to avoid title[256] → buffer overflow
	finish allocate function
	
	1. made a function get_Size that get the element's number.
	2. Made a function that allocate with nb_elem.

						   ID1       ID2
	const char *url = "https://daft.sex/watch/-45836141_456239619";
	Extract ID1 & ID2 automatically 

	1. loop throught the URL
	2. if there is / go to the next / and grab the str 
	   into group 1 → daft.sex 
	        group2  → watch
		group3  → 45836141
		group4  → 456239619

	OKAY: the position for the first ID is good.You can extract it with copy_str
	
# [DEBUG]
	define my_vars
	 displ buffer.content // display the content of buffer
	 displ *buffer->content // display current char
	 displ start
	 displ end
	 displ position
	 displ matches
	 displ occurrence[j]
	 displ j
	 displ i

	// to check in loop
	displ buffer.content[i]
	displ occurence[i]


# [SAVES]

# [ DAFT]
	const char *url = "https://daft.sex/watch/-45836141_456239619";

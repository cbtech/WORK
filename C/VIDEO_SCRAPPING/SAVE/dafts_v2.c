// Daft Scraper using Curl ( Start: July 2022)
// tcc  -g -Wall -w daftscrape.c -lcurl -o DAFTSCRAPE
// DEBIAN
// tcc -g -Wall daftscrape.c -I/usr/include/x86_64-linux-gnu -lcurl -o DAFTSCRAPE

#include <curl/curl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


typedef struct {
	char* content;
	size_t size;
} buffer_t;


size_t grow_buffer(void *content,size_t size,size_t nb_elems,void *ctx)
{
	
	size_t real_size = size * nb_elems;
	buffer_t *buffer = (buffer_t*) ctx;
	buffer->content = realloc(buffer->content, buffer->size + real_size + 1);
	if (!buffer->content) {
		fprintf(stderr,"Not enough memory(Realloc failed)\n");
		return 0;
	}
	memcpy(&(buffer->content[buffer->size]),content,real_size);
	buffer->size += real_size;
	buffer->content = buffer->content;
	return real_size;

}

void reset_buffer(buffer_t *buffer,int nElem) {
		buffer->content -= nElem;
}

void copy_str(buffer_t *buffer,char *dst,int start,int end) {
	for (int i = start,j=0; i <= end ;i++,j++) {
		dst[j] = buffer->content[i];
	}
}

void extract_tag(buffer_t *buffer,char *string,char *dest)
{
	char tmp[256];
	int start = 0,end = 0;
	char *occurence = string;
	int occurence_length = strlen(occurence);
	int occurence_found = 0;
	int matches = 0;
	int position = 0;
	int j = 0;

	// Loop through the buffer
	for( ;*buffer->content != '\0' ;buffer->content++) {
		if(*buffer->content == '<' && occurence_found) {
			// End of occurence
			end = position - 1;
			occurence_found = 0;
		}

		if (*buffer->content == occurence[j]) {
			// Check if all letters is in occurrence 
			for (int i = 0;i <= occurence_length; i++)
			{
				if(buffer->content[i] == occurence[i]) {
					matches++;
					// If the two occurences length are equal
					if (matches == occurence_length) {
						start = position + occurence_length;
						occurence_found = 1;
				}
			}
			
				else { 
					// That doesn't match
					matches = 0;
				}
			}
		}
		position++;
	}

	reset_buffer(buffer,position);
	copy_str(buffer,dest,start,end);
}

static char title[256];
char* extract_title(buffer_t *buffer)
{
	extract_tag(buffer,"<title>",&title);
	printf("TITLE: %s",title);
}

int main(int argc,char **argv)
{
	// GET FIRST ARG
	//const char *url = argv[1];
	buffer_t buffer;

	buffer.content = malloc(1);
	buffer.size = 0;

	const char *url = "https://cbtech.codeberg.page/test.html";

	CURL *curl;
	CURLcode res;

	curl = curl_easy_init();

	if(curl) {
		// READ THE CONTENT TO TTY
		//READ THE CONTENT AND EXPORT IT WHEREVER YOU WANT
		curl_easy_setopt(curl,CURLOPT_WRITEFUNCTION,grow_buffer);
		curl_easy_setopt(curl,CURLOPT_WRITEDATA,&buffer);
		curl_easy_setopt(curl,CURLOPT_URL,url);
		res = curl_easy_perform(curl);

		// PRINT BUFFER CONTENT
		extract_title (&buffer);

	}

	return 0;
}

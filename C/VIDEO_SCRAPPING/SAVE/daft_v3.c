// Daft Scraper using Curl ( Start: July 2022)
// tcc  -g -Wall -w daftscrape.c -lcurl -o DAFTSCRAPE
// DEBIAN
// tcc -g -Wall daftscrape.c -I/usr/include/x86_64-linux-gnu -lcurl -o DAFTSCRAPE

#include <curl/curl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define FALSE 0
#define TRUE 1

typedef struct {
	char* content;
	size_t size;
} buffer_t;

typedef struct {
	char *title;
	char *id_1;
	char *id_2;
} scrapper_t;

size_t grow_buffer(void *content,size_t size,size_t nb_elems,void *ctx)
{
	size_t real_size = size * nb_elems;
	buffer_t *buffer = (buffer_t*) ctx;
	buffer->content = realloc(buffer->content, buffer->size + real_size + 1);
	if (!buffer->content) {
		fprintf(stderr,"Not enough memory(Realloc failed)\n");
		return 0;
	}
	memcpy(&(buffer->content[buffer->size]),content,real_size);
	buffer->size += real_size;
	buffer->content = buffer->content;
	return real_size;

}

void rewind_buffer(buffer_t* buffer,int nElem) {
		buffer->content -= nElem;
}

// extract to start and calcul the length 
void extract_str(const char *src,char *dst,int start,int end) {
	
	for (int i = start,j=0; i <= end ;i++,j++) {
		dst[j] = src[i];
	}

	dst += '\0';
	// DOESN'T WORK
	// Ok for the start but the end is not correct
	//for(src += start; (*dst = *src) && src != end; src++,dst++) ;

}

void _strlen(const char*str) {
	int i = 0;
	for ( ; *str !='\0'; str++) { i++; }
}

void extract_title_tag(buffer_t *buffer,char *string,scrapper_t *s)
{
	int start = 0,end = 0,j = 0;
	char *occurence = string;
	int occurence_length = strlen(occurence);
	int occurence_found = FALSE;
	int matches = 0, position = 0;

	// Loop through the buffer
	for( ;*buffer->content != '\0' ;buffer->content++) {
		if(*buffer->content == '<' && occurence_found) {
			// End of occurence
			end = position - 1;
			occurence_found = FALSE;
		}

		if (*buffer->content == occurence[j]) {
			// Check if all letters is in occurrence 
			for (int i = 0;i <= occurence_length; i++)
			{
				if(buffer->content[i] == occurence[i]) {
					matches++;
					// If the two occurences length are equal
					if (matches == occurence_length) {
						start = position + occurence_length;
						occurence_found = TRUE;
					}
				}
					else { 
						// That doesn't match
						matches = 0;
					}
			}
		}
		position++;
	}

	rewind_buffer(buffer,position);
	
	// Dynamic allocation for Title 
	s->title = malloc((end - start) +1);
	extract_str(buffer->content,s->title,start,end);
}


void extract_id(const char *url,scrapper_t *scrapper) {
	// Extract the first and second ID
	const char *pattern = "https://daft.sex/watch/[ID]";	
	char delimiter = '_';
	const int START = 0;
	int position,length = 0;
	int first_id_extracted = 0, second_id_extracted = 0;

	for( ;*url !='\0'; url++,pattern++)
	{
		if(*url != *pattern) {
			// Extract first ID 
			if(!first_id_extracted) {
				for(int i = 0;url[i] != delimiter; i++) {
					length = i;
				}
				scrapper->id_1 = malloc(length+1);
				extract_str(url,scrapper->id_1,START,length);
				url += length + 1;
				first_id_extracted = TRUE;
			}
			// Extract second ID
			else if (!second_id_extracted) {
				if (*url == delimiter) url++;
				// Second ID has the same length
				scrapper->id_2 = malloc(length+1);
				extract_str(url,scrapper->id_2,START,length);
				second_id_extracted = TRUE;
			}

		}
		position++;
	}
}

void free_res(buffer_t *buffer,scrapper_t *scrapper) {

	free(buffer->content);
	free(scrapper->title);
	free(scrapper->id_1);
	free(scrapper->id_2);
}

int main(int argc,char **argv)
{
	// GET FIRST ARG
	//const char *url = argv[1];
	buffer_t buffer;
	scrapper_t scrapper;

	buffer.content = malloc(1);
	buffer.size = 0;

	const char *url = "https://daft.sex/watch/-45836141_456239619";
	//const char *url = "https://cbtech.codeberg.page/test.html";
	CURL *curl;
	CURLcode res;

	curl = curl_easy_init();

	if(curl) {
		// READ THE CONTENT TO TTY
		//READ THE CONTENT AND EXPORT IT WHEREVER YOU WANT
		curl_easy_setopt(curl,CURLOPT_WRITEFUNCTION,grow_buffer);
		curl_easy_setopt(curl,CURLOPT_WRITEDATA,&buffer);
		curl_easy_setopt(curl,CURLOPT_URL,url);
		res = curl_easy_perform(curl);

		// PRINT BUFFER CONTENT
		extract_title_tag(&buffer,"<title>",&scrapper);
		printf("Title: %s\n", scrapper.title);
		extract_id(url,&scrapper);
		printf("ID1: %s\n", scrapper.id_1);
		printf("ID2: %s\n",scrapper.id_2);
	
		}

	// Free All resources
	free_res(&buffer,&scrapper);
	return 0;
}

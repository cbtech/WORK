// Daft Scraper using curl
// tcc  -g -Wall -w daftscrape.c -lcurl -o DAFTSCRAPE
// DEBIAN
// tcc -g -Wall daftscrape.c -I/usr/include/x86_64-linux-gnu -lcurl -o DAFTSCRAPE

#include <curl/curl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


typedef struct {
	char* buffer;
	size_t size;
} memory;

size_t grow_buffer(void *content,size_t size,size_t nb_elems,void *ctx)
{
	size_t real_size = size * nb_elems;
	memory *mem = (memory*) ctx;
	mem->buffer = realloc(mem->buffer, mem->size + real_size + 1);
	if (!mem->buffer) {
		fprintf(stderr,"Not enough memory(Realloc failed)\n");
		return 0;
	}
	memcpy(&(mem->buffer[mem->size]),content,real_size);
	mem->size += real_size;
	mem->buffer = mem->buffer;
	return real_size;

}

void search_occurence(const char *buffer,char* string)
{
 	int start= 0,end= 0,i=0;
	char *occurence = string;
	int buffer_length = strlen(buffer);
	int occurence_length = strlen(occurence);
	int occurence_start=0, occurence_end=0;
	int matches = 0;
	int position = 0;
	int j = 0;
	for( ;*buffer != '\0' ;buffer++) {
		
	//	if(*buffer == '<'){
	//		start = position +1; 
	//		printf("%d",i);
	//	}
		if (*buffer == occurence[j]) {
			//Check if all letters is in occurrence 
			for (int i=0;i <= occurence_length;i++)
			{
				if(buffer[i] == occurence[i]) {
					matches++;
					if (matches == occurence_length)
						// start occurence 
						// OK note the position where the occurence start
						printf("Maybe\n");

				}
				else { matches =0; }

			}
			//			occurence_start = position;

		}
		//else if(*buffer == '>') {
		//	end = position - 1;
		//	occurence_end = end;
		//}
		position++;

	}
	for (int i = occurence_start; i >= occurence_end ;occurence_start++) {
		printf("%c",buffer[i]);
	}
	return "Hello";
}

char* extract_title(const char *buffer)
{
 	char title[256];
	search_occurence(buffer,"title");
	//return title;
}

int main(int argc,char **argv)
{
	// GET FIRST ARG
	//const char *url = argv[1];
	memory mem;
	
	mem.buffer = malloc(1);
	mem.size = 0;

	const char *url = "https://cbtech.codeberg.page/test.html";

	CURL *curl;
	CURLcode res;

	curl = curl_easy_init();

	if(curl) {
		// READ THE CONTENT TO TTY
		//READ THE CONTENT AND EXPORT IT WHEREVER YOU WANT
		curl_easy_setopt(curl,CURLOPT_WRITEFUNCTION,grow_buffer);
		curl_easy_setopt(curl,CURLOPT_WRITEDATA,&mem);
		curl_easy_setopt(curl,CURLOPT_URL,url);
		res = curl_easy_perform(curl);

		// PRINT BUFFER CONTENT
		extract_title (mem.buffer);
		
	}

	return 0;
}

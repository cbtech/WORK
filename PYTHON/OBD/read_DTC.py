#!/usr/bin/python3

import re
import serial
import time
import sys
from pudb import set_trace;
 

# OPEN SERIAL 
SERIAL_PORT_NAME="/dev/serial/by-id/usb-www.vgatemall.com_vLinker_FS_D381Z5VO-if00-port0"
SERIAL_PORT_BAUD = 9600
SERIAL_PORT_TIME_OUT = 0

VGATE = serial.Serial(SERIAL_PORT_NAME, SERIAL_PORT_BAUD)
VGATE.timeout = SERIAL_PORT_TIME_OUT
print("Serial Port: " + VGATE.name) 

# GET RESPONSE
def get_response(serial_connector,data):
    set_trace() # PUDB INVOKATION
    serial_connector.write(data)
    
    time.sleep(1)
    buffer = bytearray()
    while True:
        tmp = serial_connector.read()
        buffer.extend(tmp)
        
        if buffer == b'>' and buffer == b'OK':
            break
            print("Okay I found the END\n")
        
    buffer = re.sub(b"\x00",b"", buffer)
    string = buffer.decode("utf-8","ignore")
    lines = [s.strip() for s in re.split("[\r\n]",string) if bool(s)]
    return lines

  # INITIALIZE INTERFACE
port = serial.serial_for_url(SERIAL_PORT_NAME,parity=serial.PARITY_NONE,stopbits=1,bytesize=8,timeout=10)
Response = get_response(VGATE,b"AT Z\r")
print(Response)

# GET VOLTAGE OF OBDII CONTROLLER

# GET INTERFACE 

# CONNECTING TO CANBUS COMMUNICATION ISO9141-2

# READ VIN

# STORED DTC

# PRESENT DTC

